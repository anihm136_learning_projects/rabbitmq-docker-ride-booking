# Ride booking simulator
This project simulates the backend of a ride-bookings software, similar to Uber. It consists of independent services to 
- Accept new rides
- Simulate matching a customer with a ride
- Log the details of each ride to a database

## Ride acceptor
This service is a simple web server that accepts post requests at the endpoint `/new_ride`. Ride requests are of the form -
```json
{
    "pickup": str
    "destination": str
    "time": int
    "cost": int
    "seats": int
}
```
Once a ride request is received, the acceptor sends messages on two RabbitMQ queues to match the request with a ride and log the ride details in a database respectively

## Ride matcher
This service simulates the delay of matching a customer with a ride. It listens on a RabbitMQ queue, and sleeps for the time specified in the message (in the `time` field of the ride request) before returning a success message

## Database logger
This service logs the details of a ride into an SQLite database. It waits for messages on a RabbitMQ queue, and inserts a row into the database with the ride details given in the message

## Running the project
The entire simulation is orchestrated using docker-compose. It can be started by running `docker-compose up` in the root directory. Ride requests can then be sent to `localhost:8080`

## Notes
- The `wait-for-it.sh` script is used in the Dockerfiles to ensure that the application containers start after the RabbitMQ container, so that they are able to connect. The script ensures that a particular network address is accepting TCP connections before running the entrypoint command. Taken from https://github.com/vishnubob/wait-for-it
