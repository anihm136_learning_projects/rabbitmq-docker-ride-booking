import os
import time

import pika
import requests

server_ip = os.getenv("SERVER_HOST", "localhost")
consumer_id = os.getenv("CONSUMER_ID", "default_id")
requests.post(
    f"http://{server_ip}/new_ride_matching_consumer", json={"consumer_id": consumer_id}
)

rmq_host = os.getenv("RABBITMQ_HOST", "localhost")
rmq_connection = pika.BlockingConnection(pika.ConnectionParameters(rmq_host))
channel = rmq_connection.channel()

channel.queue_declare(queue="ride_match")


def callback(_ch, _method, _properties, body):
    time_to_sleep = int(body)
    time.sleep(time_to_sleep)
    print(f"Consumer {consumer_id}: Matched ride")


channel.basic_consume(queue="ride_match", on_message_callback=callback, auto_ack=True)

channel.start_consuming()
