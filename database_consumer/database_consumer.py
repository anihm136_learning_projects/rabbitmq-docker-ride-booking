import json
import os
import sqlite3

import pika

rmq_host = os.getenv("RABBITMQ_HOST", "localhost")
db_path = os.getenv("DB_PATH", "ride.db")

rmq_connection = pika.BlockingConnection(pika.ConnectionParameters(rmq_host))

channel = rmq_connection.channel()

channel.queue_declare(queue="db")

con = sqlite3.connect(db_path)
cur = con.cursor()
cur.execute(
    """CREATE TABLE IF NOT EXISTS rides (
    pickup varchar(255),
    destination varchar(255),
    time int,
    cost int,
    seats int
    )"""
)
con.commit()
con.close()


def callback(_ch, _method, _properties, body):
    data = json.loads(body)
    con = sqlite3.connect("ride.db")
    cur = con.cursor()
    cur.execute(
        "INSERT INTO rides VALUES (?,?,?,?,?)",
        (
            data["pickup"],
            data["destination"],
            data["time"],
            data["cost"],
            data["seats"],
        ),
    )
    con.commit()
    con.close()


channel.basic_consume(queue="db", on_message_callback=callback, auto_ack=True)

channel.start_consuming()
