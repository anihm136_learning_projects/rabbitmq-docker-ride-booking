import json
import os

import pika
from fastapi import FastAPI, Request
from pydantic import BaseModel


class Ride(BaseModel):
    pickup: str
    destination: str
    time: int
    cost: int
    seats: int


class Consumer(BaseModel):
    consumer_id: str


consumers = []


def create_app():
    app = FastAPI()
    rmq_host = os.getenv("RABBITMQ_HOST", "localhost")
    rmq_connection_params = pika.ConnectionParameters(rmq_host)

    @app.post("/new_ride")
    async def new_ride(ride: Ride):
        ride_details = ride.__dict__
        db_message = json.dumps(ride_details)
        consumer_message = str(ride.time)

        rmq_connection = pika.BlockingConnection(rmq_connection_params)
        channel = rmq_connection.channel()

        channel.queue_declare(queue="ride_match")
        channel.queue_declare(queue="db")

        channel.basic_publish(
            exchange="", routing_key="ride_match", body=consumer_message
        )
        channel.basic_publish(exchange="", routing_key="db", body=db_message)

        channel.close()

        return {"message": "Created new ride"}

    @app.post("/new_ride_matching_consumer")
    async def new_consumer(consumer: Consumer, request: Request):
        ip = request.client.host
        c = {"Name": consumer.consumer_id, "IP": ip}
        consumers.append(c)
        return {"message": "Registered new consumer"}

    return app


app = create_app()
